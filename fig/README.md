# List of Figures
> - This folder contains figures, used in [my report](../report/) and [the slides](../slides/).
> - The figures have been generated with [some Python scripts](../src/).

This folder mainly contains PNG figures and GIF animations, obtained with our script on [fractional-direction Hilbert transform](../src/fdHT.py), and other figures obtained with other scripts.
Some are simple plots of functions (e.g. the family $\cos^i(x) \sin^{m-i}(x)$ for $i = 0 \dots m$), used in [my report](../report/).

There is also [this gallery (on my website)](http://perso.crans.org/besson/internship-mva-2016/fig/gallery.html), if needed.

## g-SI steerable convolutions applied to [Stochastic Sparse Processes in 2D](Stochastic_Sparse_Processes_in_2D/)
[This folder](Stochastic_Sparse_Processes_in_2D/) contains many figures illustrating the last experiments we did, using a not-yet open-source GNU Octave / MATLAB implementation of our fractional Laplacian, directional derivatives and elementary blocks $G_{\lambda,\alpha}$.

We applied our gamma-scale-invariant steerable convolution operators to Sparse Stochastic Processes in 2D, mainly of two kinds:

 - Gaussian noise,
 - Compound Poisson noise.

We tried with $\gamma=1$ (to have the elementary block) and $\lambda = 0.3, 0.5, 0.8, 0.9$, and different angles $\alpha = 0, \pi/6, \pi/4, \pi/2, 3\pi/4, \pi$.
Some interpretations and explanations are given in [the report](../report/) (see part 5.5, pages 69-73).

There is also [this other gallery (on my website)](http://perso.crans.org/besson/internship-mva-2016/fig/Stochastic_Sparse_Processes_in_2D/gallery.html), if needed.

---

> [Main directory?](../) | [License?](../LICENSE) | [Some git statistics?](../complete-stats.txt)
> © 2016 [Lilian Besson](http://perso.crans.org/besson/)
