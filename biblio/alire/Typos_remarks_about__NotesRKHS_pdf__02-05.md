## Typos about "NotesRKHS.pdf"
- page 8, (12), dy the y should be bold
- page 15, Green's function L*L --> Green's function of L*L
- page 21, (27) N_D = { p in S'(Rd) : D{p} = 0 } and not D{q}
- page 25, (36) is wrong ! Except if we ask x,y >= 0 (as done for Brownian motion covariance, s and t are >= 0)
- page 40, ||w||_{TV} = || f ||_{L1} --> || w ||_{L1} ??
