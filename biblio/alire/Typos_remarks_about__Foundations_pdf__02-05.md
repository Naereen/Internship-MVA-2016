## Typos/remarks about "Foundations.pdf"
- Page 10 : (11.2) weaker norms --> || . ||_m with m < n
- Page 12 : missing reference [?] in the footnote 2
- Page 13 : M{f}(t) --> missing reference [?]
- Page 29 : Def.11.45 : Schatten p-norm or Schatten-p norm ? (conflict with p-norm and -1 norm)
- Page 21 : Th.11.17 : point 4., the second A in (\overline{\mathcal{X} \times \mathcal{X})' should have a different name? (it does not have the same interpretation as the first A). Same in Th.11.18
- Page 39 : Def.11.52 : f : R^d --> C or R ?
- Page 43 : 11.5 : could be improved, and the [?] links should be corrected
