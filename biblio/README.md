# Liste de réferences / List of bibliographic references
- [This BibTeX](./internship-mva-2016.bib) file listed all the references we used for our project;
- [See it as plain text (.txt)](./internship-mva-2016.txt);
- Or [it as HTML](./internship-mva-2016.html) or [as PDF](https://bitbucket.org/lbesson/internship-mva-2016/downloads/MVA16__Internship__References__Lilian_Besson__2016.en.pdf);

----

#### More ?
> See [the report](../report/), [the slides](../slides/), [the code](../src/) or [the figures](../fig/) if needed.

### Liste de réferences / List of references
> - Additionally to the BibTeX file, here are (partial) lists of references:

- [Livres / Books](./books.md),
- [Articles](./articles.md),
- [Thèses / PhD theses](./theses.md).

---

> [Main directory?](../) | [License?](../LICENSE) | [Some git statistics?](../complete-stats.txt)
> © 2016 [Lilian Besson](http://perso.crans.org/besson/)
