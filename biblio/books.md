## List of reference books
> TODO : make a nice BibTeX file instead

----

### Functional analysis
- "Analyse fonctionnelle", lecture notes, by Laurent Desvillettes (2011). http://desvillettes.perso.math.cnrs.fr/analyse_fonctionnelle59.pdf
- "Semi-norms", by Kôsaku Yosida, Springer, ISBN 978-3-662-11791-0 (1965)
- TODO? "Theory of Linear Operators in Hilbert Space", by N. I. Akhiezer and I. M. Glazman, Dover, New York (1993)
- "Functional analysis", by Walter Rudin, McGraw-Hill, ISBN 978-0-010-61988-3 (1991) [BIG 962]
- "Functional analysis", by Friegyes Riescz and Bela Sz.-Nagy, Dover, ISBN 978-0-486-66289-3 (1953) [BIG 453]

### Wavelets
- "Wavelets, algorithms and applications", by Yves Meyer (translated by Robert D. Ryan), SIAM, ISBN 0-89871-309-9 (1994)
- "A Wavelet tour of signal processing", by Stephane Mallat (English and French version), XXX, XXX
- ?

### Fourier
- "Methods of Applied Fourier Analysis", by Jayakumar Ramanathan, Birkhauser, ISBN 0-8176-3963-2
- "Introduction to Fourier Analysis on Euclidean Spaces", by Elias M. Stein and Guido Weiss, Princeton (1971)
- ?

### Sparse signals and processes
- "An introduction to sparse stochastic processes", by Michael Unser and Pouya Tafti, http://www.sparseprocesses.org/ (2013)

### Compressed sensing
- "A Mathematical Introduction to Compressive Sensing", by Simon Foucart and Holger Rauhut, Springer (2013)

### Others
- ?
