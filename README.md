# Internship 2016 - Master MVA (2015-2016)
> (Fr) Ce [dépôt git](https://en.wikipedia.org/wiki/Git) contient les sources (LaTeX et Python, notamment) des travaux effectués pour mon stage de recherche de fin de master 2.
Pendant ma dernière année de formation à l'[ENS Cachan](http://www.ens-cachan.fr/), en 2015-2016, j'ai suivi le master MVA.

> - [Informations sur le master MVA](http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/) et [aussi en anglais ?](www.cmla.ens-cachan.fr/version-anglaise/academics/mva-master-degree-227777.kjsp).
> - [Plus d'informations sur le site de l'Université Paris-Saclay](http://www.universite-paris-saclay.fr/fr/formation/master/m2-mathematiques-vision-apprentissage).

## Informations sur le stage / Information about my internship
- *Où / where* : [équipe LIB / BIG team](http://bigwww.epfl.ch/), [EPFL](http://www.epfl.ch/), [Lausanne](https://fr.wikipedia.org/wiki/Lausanne) en [Suisse](https://fr.wikipedia.org/wiki/Suisse) / in [Switzerland](https://en.wikipedia.org/wiki/Switzerland)
- *Quand / when* : avril / April 2016 → août / August 2016,
- *Avec qui / with whom* : [Julien Fageot](http://bigwww.epfl.ch/fageot/) et / and [Michael Unser](http://bigwww.epfl.ch/unser/),
- *Thèmes / themes* : splines, ondelettes / wavelets, espaces et normes de Sobolev / Sobolev spaces and norms, TV-L1 and L1 minimization, théories des opérateurs / operators theory, etc.
- *Sujet / topic* : **« A theoretical study of steerable convolution operators, and possible applications to sparse processes for 2D images »**.
- *Note finale / final grade* : I got **20/20** for my internship (as given by my supervisors) and **18.43/20** for my average grade for my Master degree. Sweet !

----

## Sous-dossiers / Folders :
- [biblio](./biblio): réferences / bibliographic references,
- [report](./report): rapport de stage / internship report (LaTeX source et/and PDF) (*completely done*). Le document se trouve ici / the document is here : https://goo.gl/xPzw4A
- [slides](./slides): slides pour la présentation finale / final oral presentation (LaTeX source et/and PDF) (*completely done*). Le document se trouve ici / the document is here : https://goo.gl/vm8WPF

### Code
- [src](./src): programmes et scripts / source code and scripts (*not much*),
- [fig](./fig): figures et/and illustrations (*too much*). [See this gallery (on my website)](http://perso.crans.org/besson/internship-mva-2016/fig/gallery.html).

----

### Abstract [du rapport](https://goo.gl/xPzw4A) / Abstract of [my report](https://goo.gl/xPzw4A)
> This report sums up and presents the research I did between April and August 2016,
> in theoretical function analysis and operator theory.
> We focus mainly on two aspects: convolution operators in any dimension; and steerable convolution operators in two dimensions, for images, and their possible applications.
> Most of our results are valid regardless of the dimensions, so we try to keep a general setting as long as we can, and then we restrict to operators on 2D images for the study of steerable operators.


> We start by recalling the common notations
> used in signal processing, splines theory and functional analysis,
> and then by recalling the main properties of a fundamental tool for this domain, the Fourier transform F.
> We present the main goals of our research, in order to motivate the interests of such a theoretical study of convolution operators from a practical point of view.
> We chose to follow a very didactic approach, and so we redefine "from scratch"
> the theory of functional operators, along with the most important results.
> Our operators can have structural as well as geometric properties,
> namely linearity or continuity, and translation-, scaling-, rotation-invariance, unity
> -- all these properties being already well-known -- or steerability.


> However, we study extensively the links between all these properties, and we present many theorems of characterizations of different properties.
> To the best of our knowledge, this document is the first attempt to summarize all these results,
> and some of our latest characterizations seem to be new results.


> After a very broad section on operators, we focus on steerable convolution operators G, mainly in 2D,
> as they appear to be the natural framework for shape- and contour-detection operators for images.
> Our main results consist on characterizations of steerable convolution operators, first written as a sum of modulated and iterated real Riesz transforms.
> Then adding the gamma-scale-invariance gives a nicer form, as a composition of a fractional Laplacian (-Delta)^{gamma/2}, some directional derivatives D_{alpha_i} and an invertible  part G0,
> and another form as a composition of elementary blocks that all have the same form G_{lambda, alpha}.
> This last form is very appealing for implementation, as it is enough to program the elementary block, and to compose it to obtain every 2D steerable gamma-scale-invariant (gSI) convolution operator,
> and has a strong theoretical interpretation: a G gSI and steerable of order n_G gets
> decomposed as a product of elementary blocks, all 1-SI steerable of order 1 or 2.


> We conclude by presenting the results of some experiments on 2D stochastic processes,
> in order to illustrate the effects of our elementary blocks as well as more complicated operators.
> We highlight some properties on the examples, like their trade-of between the directionality of D_{alpha_i} and the isotropy of (-Delta)^{gamma/2}.
> Our operators could also be used to develop new splines (ie new sampling schemes), and new Green's functions (\ie new denoising and data recovery algorithms),
> but we did not have the time to fully study these aspects.

----

## À propos / about
### Merci / Thank you
- Merci grandement à / Thanks a lot to : Michael Unser & Julien Fageot (advisors), Virginie Uhlmann (collaborator), Thibault Groueix ("desktop-mate")
- Merci à tous les membres du LIB à l'EPFL (Laboratoire d'Imagerie Biomédicale) / Thanks to all the members of the BIG team at EPFL (Biomedical Imaging Group)
- Merci spécial à / Special thanks to : Gabriel Peyré

### Copyright
- (Fr) Tout le contenu de ce dépôt git est ma propriété, © 2016, [Lilian Besson](http://perso.crans.org/besson/) (sauf mentions contraires).
- (En) All the content in this git repository is my property, © 2016, [Lilian Besson](http://perso.crans.org/besson/) (except if stated otherwise).

### Licence / License :
[![Creative Commons License - CC-BY 4.0](https://i.creativecommons.org/l/by/4.0/88x31.png)](http://creativecommons.org/licenses/by/4.0/)

- (Fr) Ce projet entier est placé sous [la licence Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) (CC BY 4.0).
- (En) This entire project is publicly released under [the Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/) (CC BY 4.0).
