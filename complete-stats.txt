
Commit stats :
git-complete-stats.sh: do statistics on this git repository.
  From https://github.com/esc/git-stats/

Number of commits per author:
   187	Lilian Besson

----------------------
Statistics for: Lilian
  - Number of files changed: 447
  - Number of lines added: 39061
  - Number of lines deleted: 17759
  - Number of merges: 0

Calendar :
    Aug Sep           Oct       Nov     Dec     Jan       Feb     Mar     Apr       May     Jun     Jul       Aug 
                                                                            .     .   .                   . O . 
    M                                                                     . . .   . .     . .     . .     o   . 
                                                                                  .       .       . . . . o 0   
    W                                                                   o   .             .       . . . . . O   
                                                                        . .       .       .   .   . . . o O O   
    F                                                                   . .             . .       . .   . 0 .   
                                                                      o                                 . o     

                                                                                          Less   . o O 0  More
 187: Total commits
  13: Days ( Aug  9 2016 - Aug 21 2016 ) - Longest streak excluding weekends       
  13: Days ( Aug  9 2016 - Aug 21 2016 ) - Longest streak including weekends       
   6: Days ( Aug 23 2016 - Aug 29 2016 ) - Current streak                          
