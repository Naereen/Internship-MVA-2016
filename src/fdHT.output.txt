
- Starting turning animation of the simple-fdHT ...
+ Plotting the 5th figure ... (plot_color = True)
Trying to maximize a matplotlib window with fullWindow(): backend = GTK3Agg
Using theta = -3.141592653589793
Starting apply_fdHT(image, tau, theta, u, n, size) ...
1. n = 2 and size = 512
2. Shape of image : (512, 512)
3. Shape of tau : () : tau =  -0.5
4. Shape of theta : () : theta =  -3.141592653589793
5. Shape of u : (2,) : u =  [ -1.00000000e+00  -1.22464680e-16]
6. Shape of fourier_image : (512, 512)
7. Shape of freqs : (512,)
8. Shape of omega : (512, 512, 2)
9. Using one_fdHT with tau = -0.5 and u = [ -1.00000000e+00  -1.22464680e-16].
10. Shape of fourier_multiplier : (512, 512)
11. Shape of transformed_image : (512, 512)
    And dtype of transformed_image : complex128
Complex2HSV(z, ...): z.dtype = complex128, rmin = 0.00814 and rmax = 1.08
Starting the animation...
Starting apply_fdHT(image, tau, theta, u, n, size) ...
1. n = 2 and size = 512
2. Shape of image : (512, 512)
3. Shape of tau : () : tau =  -0.5
4. Shape of theta : () : theta =  -3.14159265359
5. Shape of u : (2,) : u =  [ -1.00000000e+00  -1.22464680e-16]
6. Shape of fourier_image : (512, 512)
7. Shape of freqs : (512,)
8. Shape of omega : (512, 512, 2)
9. Using one_fdHT with tau = -0.5 and u = [ -1.00000000e+00  -1.22464680e-16].
10. Shape of fourier_multiplier : (512, 512)
11. Shape of transformed_image : (512, 512)
    And dtype of transformed_image : complex128
Complex2HSV(z, ...): z.dtype = complex128, rmin = 0.00814 and rmax = 1.08

---> OK for the turning animation of the simple-fdHT.
TODO: finish this script !
Complex2HSV(z, ...): z.dtype = complex128, rmin = 0.00138 and rmax = 1.41
