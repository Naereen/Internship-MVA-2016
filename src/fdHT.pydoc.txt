Help on module fdHT:

NAME
    fdHT - Test of one or more Fractional-Directional Hilbert Transform (fdHT) applied to test images in 2D.

FILE
    /home/lilian/ownCloud/cloud.openmailbox.org/Stage_MVA_2016/src/fdHT.py

DESCRIPTION
    About:
    
    - This script is a companion of my internship report, see https://bitbucket.org/lbesson/internship-mva-2016.
    - fdHT and details on the implementation are given in the part A.3.5 of my report, cf. https://goo.gl/xPzw4A
    
    
    Remarks:
    
    - Only square n-dim images are used (of shape (size,)*n = (size, size, .., size)).
    - The dimension n is left free in the script, but we only intend to use it with n = 2.
    - See this page for some details about fftn and ifftn: https://docs.scipy.org/doc/numpy/reference/routines.fft.html#higher-dimensions
    
    
    About:
    
    - *Date:* Friday 24 June 2016.
    - *Author:* Lilian Besson, for the MVA Master, (C) 2015-16.
    - *Licence:* MIT Licence (http://lbesson.mit-license.org).

FUNCTIONS
    apply_and_plot(tau=0, theta=None, offset=None, save=True, lena=False, plot_fdht=True, plot_fourier=False, plot_fdht_fourier=False, plot_fourier_color=False, plot_color=False)
        Test the apply_fdHT(tau, theta) on a generated image, display both the original and transformed image, and save it.
    
    apply_and_turn(offset=None, lena=False, save=True, turn_input=False, move_tau=False, theta0=-1.5707963267948966, tau0=-0.5)
        Test the apply_fdHT(tau, theta) on a generated image, display both the original and transformed image, for:
        
        - theta = [0, 2 pi] slowly turning if not turn_input nor move_tau, with tau = tau0,
        - tau = [-2, 2] slowly growing if move_tau, with theta = theta0,
        - for a turning input (half-plane) if turn_input, with theta = theta0 and tau = tau0.
    
    apply_fdHT(image, tau=0, theta=None, u=None, n=None, size=None, returnFourier=False, use_rfft=False)
        Apply one or more n-dimensional Fractional-Directional Hilbert Transform to the image, on the Fourier domain (and coming back with ifftn).
        
        .. math:: \mathcal{H}_{\tau, \overrightarrow{u}} \{f\} (x) \leftrightarrow_{\mathcal{F}} \widehat{\mathcal{H}_{\tau, \overrightarrow{u}} f}(\omega) = \exp(j \pi \tau \mathrm{sign}(\langle \overrightarrow{u}, \omega \rangle)) \widehat{f}(\omega).
        
        - Default values for tau is 0 (in which case u or theta does not matter: H = Id).
        - If n and size are absent, we use the dimension and size of the image.
        
        - If theta is given, :math:`u = u_{\theta} := [ \cos(\theta), \sin(\theta) ]` is used instead of u (in the n = 2 setting).
        
        - If returnFourier, the Fourier transformed image is returned instead of the completely transformed image.
        
        - XXX If use_rfft, Experimentally using rfftn because the input is REAL (cf. https://docs.scipy.org/doc/numpy/reference/routines.fft.html#real-ffts).
    
    black(X)
        Dummy generator for our 'interest' image: a simple black (= 1) background of correct size.
    
    centered_gaussian(X)
        Default generator for our 'interest' image: a simple n-dim Gaussian image, centered in (0, 0) and localized on [-4, 4], of variance 1.
        
        - X is an array of shape (n, size, ..., size) where X[:, i1, ..., in] is the n-dimensional (vectorial) value at the location (i1, ..., in) in the meshgrid.
        - In practice, n = 2 and so X[:, i, j] has 2 components: x[i, j] and y[i, j] (or a complex component).
    
    centered_laplace(X)
        Generator for our 'interest' image: a simple n-dim Laplace image, centered in (0, 0) and localized on [-4, 4], of variance 1.
        
        - X is an array of shape (n, size, ..., size) where X[:, i1, ..., in] is the n-dimensional (vectorial) value at the location (i1, ..., in) in the meshgrid.
        - In practice, n = 2 and so X[:, i, j] has 2 components: x[i, j] and y[i, j] (or a complex component).
    
    default_generator = centered_gaussian(X)
        Default generator for our 'interest' image: a simple n-dim Gaussian image, centered in (0, 0) and localized on [-4, 4], of variance 1.
        
        - X is an array of shape (n, size, ..., size) where X[:, i1, ..., in] is the n-dimensional (vectorial) value at the location (i1, ..., in) in the meshgrid.
        - In practice, n = 2 and so X[:, i, j] has 2 components: x[i, j] and y[i, j] (or a complex component).
    
    generate_example(n=2, size=1024, f=<function centered_gaussian>, amplitude=2, origin=0)
        Create an 'interest' square n-dimensional image, by applying f to a grid of shape (size,)*n, on [-amplitude, amplitude] in each dimension (centered in origin, defaut is 0).
    
    main(lena=False, animation=False, multi=False, save=False, turn_input=False, move_tau=False, **kwargs)
        Main function, launches the experiments.
    
    multi_fdHT(omega, tau, u)
        Compute the Fourier response of a composition of K n-dimensional Fractional-Directional Hilbert Transform: ::
        
        ^H_tauk,uk (omega) = exp(j * pi * (tau_1 * sign(< omega , u_1 >) + ... + tau_K * sign(< omega , u_K >)).
        
        
        .. math:: \widehat{\mathcal{H}_{\tau_{1:K}, \overrightarrow{u}_{1:K}}}(\omega) = \exp\bigl(j \pi \sum\limits_{k=1}^{K} \tau_k \mathrm{sign}(\langle \omega , \overrightarrow{u_k} \rangle)\bigr).
        
        - It is applied to the frequency array omega, with parameters tau and u.
        - omega has shape (size, ..., size, n) and u has shape (n,) : the dot product apply to the last coordinate.
    
    one_fdHT(omega, tau, u)
        Compute the Fourier response of ONE n-dimensional Fractional-Directional Hilbert Transform: ::
        
        ^H_tau,u (omega) = exp(j * pi * tau * sign(< omega , u >)).
        
        
        .. math:: \widehat{\mathcal{H}_{\tau, \overrightarrow{u}}}(\omega) = \exp(j \pi \tau \mathrm{sign}(\langle \omega , \overrightarrow{u} \rangle)).
        
        - It is applied to the frequency array omega, with parameters tau and u.
        - omega has shape (size, ..., size, n) and u has shape (n,) : the dot product apply to the last coordinate.
        
        Special cases:
        
        - Use tau = 1/2 and u = [cos(theta), sin(theta)] to get the natural n-dimensional Directional Hilbert Transform.
        - Use tau = 1 [mod 2] to get - Identity.
        - Use tau = 0 [mod 2] to get + Identity.
    
    rotated_half(X, theta=0.39269908169872414, smooth=False)
        Generator for our 'interest' image: a image, white on one half and black on the other half, separated by a plane of angle theta.
        
        - X is an array of shape (n, size, ..., size) where X[:, i1, ..., in] is the n-dimensional (vectorial) value at the location (i1, ..., in) in the meshgrid.
        - In practice, n = 2 and so X[:, i, j] has 2 components: x[i, j] and y[i, j] (or a complex component).
    
    savegifs(offset=0)
        Generating and saving a lot of animations as gif files...
    
    sleep(...)
        sleep(seconds)
        
        Delay execution for a given number of seconds.  The argument may be
        a floating point number for subsecond precision.
    
    white(X)
        Dummy generator for our 'interest' image: a simple white (= 0) background of correct size.

DATA
    AMPLITUDE = 2
    absolute_import = _Feature((2, 5, 0, 'alpha', 1), (3, 0, 0, 'alpha', 0...
    division = _Feature((2, 2, 0, 'alpha', 2), (3, 0, 0, 'alpha', 0), 8192...
    print_function = _Feature((2, 6, 0, 'alpha', 2), (3, 0, 0, 'alpha', 0)...


