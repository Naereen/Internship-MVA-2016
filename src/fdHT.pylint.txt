************* Module fdHT
W:119, 0: TODO? Improve support for non-square image! (fixme)
W:214, 0: TODO Comment/uncomment here to change the default one (fixme)
W:248, 0: XXX To shift the center of the image (fixme)
W:280, 0: XXX should be the same colorbar for each plot! (fixme)
W:287, 0: XXX should be the same colorbar for each plot! (fixme)
W:292, 0: XXX should be the same colorbar for each plot! (fixme)
W:297, 0: XXX should be the same colorbar for each plot! (fixme)
W:313, 0: XXX should be the same colorbar for each plot! (fixme)
W:318, 0: XXX should be the same colorbar for each plot! (fixme)
W:325, 0: XXX should be the same colorbar for each plot! (fixme)
W:330, 0: XXX should be the same colorbar for each plot! (fixme)
W:363, 0: XXX should be the same colorbar for each plot! (fixme)
W:369, 0: XXX should be the same colorbar for each plot! (fixme)
W:374, 0: XXX should be the same colorbar for each plot! (fixme)
W:379, 0: XXX should be the same colorbar for each plot! (fixme)
W:412, 0: XXX should be the same colorbar for each plot! (fixme)
W:419, 0: XXX should be the same colorbar for each plot! (fixme)
W:425, 0: XXX should be the same colorbar for each plot! (fixme)
W:432, 0: XXX should be the same colorbar for each plot! (fixme)
W:456, 0: XXX should be the same colorbar for each plot! (fixme)
W:461, 0: XXX should be the same colorbar for each plot! (fixme)
W:480, 0: TODO Change here the default values for the parameter theta (fixme)
W:481, 0: TODO Change here the default values for the parameter tau (fixme)
W:494, 0: TODO To shift the center of the image, if you need (fixme)
W:510, 0: XXX should be the same colorbar for each plot! (fixme)
W:523, 0: XXX should be the same colorbar for each plot! (fixme)
W:524, 0: XXX Should we plt.show() before the animation? (fixme)
W:525, 0: XXX It seems to work sometimes but maybe not mandatory (fixme)
W:569, 0: XXX Should we plt.show() inside the animation? (fixme)
W:576, 0: XXX Should we plt.show() before the animation? (fixme)
W:597, 0: XXX Should we plt.show() before the animation? (fixme)
W:622, 0: TODO Change here to play with the amplitude of the tau_i (fixme)
W:637, 0: TODO Comment/uncomment the next lines to control which plot is included: (fixme)
W:699, 0: TODO Comment/uncomment here to change the different settings of the simulation (fixme)
W:708, 0: TODO: keep working A LITTLE BIT on this script (but not too much) ! (fixme)
W: 32, 7: Catching too general exception Exception (broad-except)
W:237,38: Redefining name 'offset' from outer scope (line 698) (redefined-outer-name)
R:237, 0: Too many local variables (31/15) (too-many-locals)
R:237, 0: Too many branches (23/12) (too-many-branches)
R:237, 0: Too many statements (182/50) (too-many-statements)
W:480,19: Redefining name 'theta0' from outer scope (line 702) (redefined-outer-name)
W:478,19: Redefining name 'offset' from outer scope (line 698) (redefined-outer-name)
R:478, 0: Too many local variables (29/15) (too-many-locals)
W:606, 4: Using a conditional statement with a constant value (using-constant-test)
W:601, 8: Unused variable 'anim' (unused-variable)
W:621, 4: Redefining name 'offset' from outer scope (line 698) (redefined-outer-name)
W:670,13: Redefining name 'offset' from outer scope (line 698) (redefined-outer-name)


Report
======
411 statements analysed.

Statistics by type
------------------

+---------+-------+-----------+-----------+------------+---------+
|type     |number |old number |difference |%documented |%badname |
+=========+=======+===========+===========+============+=========+
|module   |1      |1          |=          |100.00      |100.00   |
+---------+-------+-----------+-----------+------------+---------+
|class    |0      |0          |=          |0           |0        |
+---------+-------+-----------+-----------+------------+---------+
|method   |0      |0          |=          |0           |0        |
+---------+-------+-----------+-----------+------------+---------+
|function |14     |14         |=          |100.00      |21.43    |
+---------+-------+-----------+-----------+------------+---------+



External dependencies
---------------------
::

    complexplot (fdHT)
    matplotlib (fdHT)
      \-animation (fdHT)
      \-pyplot (fdHT)
    numpy (fdHT)
      \-fft (fdHT)
    scipy 
      \-misc (fdHT)



Raw metrics
-----------

+----------+-------+------+---------+-----------+
|type      |number |%     |previous |difference |
+==========+=======+======+=========+===========+
|code      |444    |62.45 |443      |+1.00      |
+----------+-------+------+---------+-----------+
|docstring |94     |13.22 |94       |=          |
+----------+-------+------+---------+-----------+
|comment   |114    |16.03 |114      |=          |
+----------+-------+------+---------+-----------+
|empty     |59     |8.30  |59       |=          |
+----------+-------+------+---------+-----------+



Duplication
-----------

+-------------------------+------+---------+-----------+
|                         |now   |previous |difference |
+=========================+======+=========+===========+
|nb duplicated lines      |0     |0        |=          |
+-------------------------+------+---------+-----------+
|percent duplicated lines |0.000 |0.000    |=          |
+-------------------------+------+---------+-----------+



Messages by category
--------------------

+-----------+-------+---------+-----------+
|type       |number |previous |difference |
+===========+=======+=========+===========+
|convention |0      |2        |-2.00      |
+-----------+-------+---------+-----------+
|refactor   |4      |4        |=          |
+-----------+-------+---------+-----------+
|warning    |43     |43       |=          |
+-----------+-------+---------+-----------+
|error      |0      |0        |=          |
+-----------+-------+---------+-----------+



Messages
--------

+---------------------+------------+
|message id           |occurrences |
+=====================+============+
|fixme                |35          |
+---------------------+------------+
|redefined-outer-name |5           |
+---------------------+------------+
|too-many-locals      |2           |
+---------------------+------------+
|using-constant-test  |1           |
+---------------------+------------+
|unused-variable      |1           |
+---------------------+------------+
|too-many-statements  |1           |
+---------------------+------------+
|too-many-branches    |1           |
+---------------------+------------+
|broad-except         |1           |
+---------------------+------------+



Global evaluation
-----------------
Your code has been rated at 8.86/10 (previous run: 8.80/10, +0.05)

