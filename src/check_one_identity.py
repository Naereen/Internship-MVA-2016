#! /usr/bin/env python
# -*- coding: utf-8; mode: python -*-
"""
A Python 2/3 script to test one numerical identity.

- *Date:* May 03 2016.
- *Author:* Lilian Besson, for the MVA Master, (C) 2015-16.
- *Licence:* MIT Licence (http://lbesson.mit-license.org).
"""

from __future__ import print_function, division  # Python 2 compatibility if needed
# import numpy as np
import random


#: Number of random tests to do
N = 100000


# Some random generators
def generate1():
    """U([0, 1])"""
    return random.random()


def generate2():
    """U([|-100, 100|])"""
    return random.randint(-100, 100)


def generate3():
    """U([-1000, 1000])"""
    return -1000 + (random.random() * 2000)


# The core function
def checkIt(generate, verb=False, tol=1e-12):
    """min(x, y) = 1/2 ( |x| + |y| - |x-y| )"""
    x = generate()
    y = generate()
    # lhs = 2 * min(x, y)
    lhs = 2 * min(abs(x), abs(y))
    # rhs = abs(x) + abs(y) - abs(abs(x) - abs(y))
    # rhs = (x + y) - abs(x - y)
    rhs = abs(x) + abs(y) - abs(x - y)
    # res = lhs == rhs
    res = abs(lhs - rhs) < tol
    if not res and verb:
        print("For x = {:g} and y = {:g} ...".format(x, y))
        # print("    lhs = 2 * min(|x|, |y|) = {:g}".format(lhs))
        print("    lhs = 2 * min(x, y) = {:g}".format(lhs))
        print("    rhs = |x| + |y| - |x-y| = {:g}".format(rhs))
    return res


if __name__ == '__main__':
    print("Script to test one identity, by Lilian Besson (C) 2016")
    print("MIT Licensed (http://lbesson.mit-license.org)")
    print("\nUsing N = {}.\n".format(N))
    # Start
    print("Inspecting the identity {:s} :".format(checkIt.__doc__))
    for g in [generate1, generate2, generate3]:
        t = 0
        for i in range(N):
            if checkIt(g):
                t += 1
        print("  - For x, y ~ {:s}, the identity seems to be true {:.2%} of the time...".format(g.__doc__, t / N))
    # DONE
    print("\nFinished for this script 'check_one_identity.py'.")

# End of check_one_identity.py
