# Programmes et scripts / Source code and scripts
> These programs are written in [Python](https://www.python.org/), and are valid with both [version 2](https://docs.python.org/2/) (2.7+) and [version 3](https://docs.python.org/3/) (3.4+).

## Fractional-directional Hilbert transform
- [fdHT.py](fdHT.py): implements the fractional-directional Hilbert transform (**fdHT**), in any dimension, and creates many illustrations of static (`.png`) or animated (`.gif`) use of the fdHT for 2D images. See [in the figure folder](../fig/).

- [complexplot.py](complexplot.py): produces [this figure](../fig/complexplot.png), explaining the color-map used to illustrate a complex-valued 2D operator (the fdHT for non-zero `tau`).

## Other scripts
- [plot_cos^k_times_sin^n-k.py](plot_cos^k_times_sin^n-k.py): plots the family of (free) functions `x -> cos^k(x) * sin^{n-k}(x)` for `x = 0 .. 2 pi` and `k = 0 .. n`. See [in the figure folder](../fig/). I did the proof of their linear independance manually, but these plots were also useful to understand better their behavior.

- [check_one_identity.py](check_one_identity.py): a dummy script to numerically check some formal identities involving absolute values and maximum, for real numbers. It was just to play a little bit with Python (in April).

---

> [Main directory?](../) | [License?](../LICENSE) | [Some git statistics?](../complete-stats.txt)
> © 2016 [Lilian Besson](http://perso.crans.org/besson/)
