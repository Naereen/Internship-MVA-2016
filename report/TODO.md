# TODO : report

- DONE check every ``FIXME`` and see if I can easily fix it ?

## Section 1 - Introduction - DONE
> I found that it however lacks a "proper" introduction, like one would expect in a scientific article.


DONE Change the introduction as Gabriel suggested it

> It should include motivations (in particular "why steerability"), previous works and a precise statement of contributions (and maybe some more about previous works related to the proposed contributions).

FIXED, except citing more previous work. La flemme...

> In particular, I would expect to find the definition of steerability (perhaps informal) and its importance already in the introduction. To me, the Section "Introduction" does not really reads like an introduction.

FIXED


> I found that citations to many previous works on steerability and directional filterings are lacking (e.g. Simoncelli, directional filters, steerable wavelets, curvelets, dual tree, etc)

XXX Add references to previous work on that aspect !?

> and should be put in perspective in the introduction.

Ca me prendrait trop de temps, on le fera dans l'article

> I have the impression section 2 and 3 are classical results, although there is no previous works reported. For instance, it seems quite related (equivalent) to the characterization of the fractional brownian motion as a scale-invariant process.

XXX Add references to previous work ! La flemme... Et je le fais deja beaucoup ?


> It would be nice to have an explicit formula for the steerability coefficients (i.e. how to express the output of an arbitrary rotated operator as a function of a few fixed orientations), specially because it is one of the most important practical use of steerability in computer vision.

DONE in end of section 4 ! Je vois pas le but dans notre contexte, mais soit. Je le ferai...


## Section 2 - DONE ?

- For Section.2 (Cor. 2.22, 2.30, 2.35) about the linear sub-space, DONE? but we should specify that they are TOPOLOGICAL linear sub-space, and the topology is preserved in a similar manner than the linearity. https://en.wikipedia.org/wiki/Topological_vector_space DONE? \cite{grothendieck73,bourbaki81}
- XXX Define properly the class of *invertible operators* ? La flemme...
- TODO change ``linear sub-space'' to ``topological linear sub-space'' in all the results ?


## Section 4 - DONE

> Coefficients of steerability as a function of the angle (i.e. how to rotate) in some basis (in particular how to select such a basis -- equispaced angles?) ?

DONE Facile, avec le mini calcul qu'on a fait l'autre jour (R_theta_0 va donner a_k --> a_j e^{j k theta_0}).


> I am surprised there is no exact formula for n_G, and only a loose bound n_G<=N_G+1.

DONE Je dois justifier rapidement la formule et l'inclure :
n_G = 1 + nb de termes qui ne sont pas purement isotrope
n_G = 1 + nb de termes qui sont "au moins un peu" directionnel


> - Affine invariance ? other group actions? 3-D steerability? difficulty is non-commutativity? non-compactness?
> - any discrete theory of steerability ?

Bonnes questions mais ça va au delà de notre étude.
DONE Je les signalerai dans une remarque en fin de partie 2, mais rien de plus...


> - Projection on the set of steerable filters ?
> - Non linear operator steerability ?

??
DONE Idem


## Section 5 - TODO?
- DONE §5 include the figures and experiments used in my slides instead. lambda = 0 and 1 has to be included !
- DONE better captions and descriptions for the figure. Remove old figures

- DONE §5 has to discuss at least a little bit about the form "L s = w" ?? Je sais pas trop quoi dire...

- TODO in §5, some things are missing:
   + XXX a subsection on operators and inversions... XXX j'aurai pas le temps !
     * DONE with refs and explanations for the "known cases" of $(-Delta)^{\gamma/2}$ and $D_{\alpha}$ -- I only gave a reference, very quickly... XXX
     * and discussion about the inversion of $G_{\lambda, \alpha}$ when $\lambda \neq 0,1$, and explain the difficulty in computing the close form of its Green's function... XXX pas le temps !
   + DONE the definition of the process relies on the specification of **a** valid left-inverse, this HAS TO BE explained!

- TODO change order of elements between end of §4 and §5... Move the theorems of decomposition from §5 to the end of §4. XXX La flemme, et je trouve que ça a quand même une certaine cohérence comme ça !

----

## Bitbucket
- XXX do some commits regularly with online editor to save my changes to bitbucket...


## DONE

- DONE draft for the email to Delphine, with links to the slides and report (goo.gl/...)
- in the proof for Th.4.31 (Heart, sum decomposition), DONE justify better (with weighted Sobolev spaces)
- DONE in §5, explain the jump law of our Compound Poisson, and add a link to the Wikipedia article (https://en.wikipedia.org/wiki/Compound_Poisson_process)
- DONE Be less absolute, for all the parts concerning the consequence of our results or the extent of our bibliographic search
- DONE give and (quickly) prove a formula n_G = 1 + max_i { m_i }, for the "Weak converse of proposition 4.42", prop.4.46 (p59)
- DONE Need to clarify that F can be defined on : S' -> S', S -> S, L2 -> L2. Consequence: G{f} is in S' so F{G{f}} is well defined
- FIXED footnote in remark --> footnotemark / footnotetext{...}

- Added the example of generalized Riesc transform: hat(g)(\bw) = e^{j beta theta} with beta non-integer. FIXME is it really steerable? If so, how does it behave with the carac of sum a_k e^(j k theta) : if beta irrational, infinite sum ??? Osef...
